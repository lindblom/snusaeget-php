<?
// Logga in i databasen
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

include("loginchecker.php");

mysql_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>råtobak, snussatser och snusaromer : snusaeget.se</title>
	<meta name="description" content="I vår webshop säljer vi råtobak i olika former för tillverkning av eget snus och egen röktobak till rätt pris med hög kvalité och snabba leveranser. Kolla in vårt bonussystem. snusaeget.se">
	<meta name="keywords" content="råtobak, tobak, snus, cigaretter, eget snus, snustillverkning, snussats, snusarom, röktobak, piptobak, skattefri, virginia">
	<link href="stilen.css" rel="stylesheet" type="text/css">
	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
	</script>
</head>
	<body>
	  <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-827328-10']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
		<div id="page_div">
			<? include("logo_div.php"); ?>
			<? include("menu_div.php"); ?>
			<div id="content_div">
				<center>
					<div style="width:500px; text-align:left; border:1px #006633 dashed; background:#FFFFFF; padding:5px;">
						<div style="font-weight:bold; text-align:center;">Snussats till f&auml;rdigt snus i 4 enkla steg </div>
						<div style="font-weight:bold;">1.</div>
						<div style="padding-left:15px; padding-bottom:10px;">Koka upp 4 dl vatten  för 250 gram snuspulver, tillsammans med en påse  koksalt. Var noga med att saltet löser sig. När det saltade vattnet har svalnat till ca 50 grader kan du börja blanda i snuspulvret. Låt gärna blandningen stå och dra någon timme, innan du rör om blandningen ordentligt. Kärlet du använder kan vara av plast, glas, porslin eller en rostfri bunke. Som avslutning täcker du hela kärlet med aluminiumfolie, se till att det blir ljustätt.</div>
						<div style="font-weight:bold;">2.</div>
						<div style="padding-left:15px; padding-bottom:10px;">Kärlet med snuspulverblandningen ska nu ställas på en plats som har en temperatur av ca 50 grader, där skall blandningen svettas i 5 dygn. Har du inte tillgång till ett pannrum eller värmeskåp, kan du i värsta fall använda kökets bakugn, kanske inte alltför populärt av andra matsugna familjemedlemmar. Du kan bygga en värmelåda av en vanlig kartong som du invändigt klär med frigolit, även locket tillverkas av frigolit. Värmekällan kan vara en vanlig glödlampa på 25, 40 eller 60 W beroende på årstid och utrymmets temperatur.<br /><br />Du bör röra om kärlet ordentligt en gång om dagen, blir blandningen för torr, kokar du upp och blandar i
1 dl vatten.</div>
						<div style="font-weight:bold;">3.</div>
						<div style="padding-left:15px; padding-bottom:10px;">Efter fem dagar tar du ut kärlet och blandar i en påse natriumkarbonat. natriumkarbonatet häller du direkt ner i snusmassan. Rör om ordentligt så du är säker på att det fördelat sig i hela snusmassan. Blanda i en flaska glycerol/propylenglykol. Du kan nu också smaksätta snuset om du inte vill ha helt okryddat snus.
Titta på våra snusaromer. Blanda och rör om ordentligt. <i>(Eventuellt snusarom går även bra att tillsätta direkt i dosan om man är osäker på resultatet och inte vill riskera hela satsen)</i></div>
						<div style="font-weight:bold;">4.</div>
						<div style="padding-left:15px; padding-bottom:10px;">Ditt snus är nu färdigt för att brukas. Är fuktighetshalten för hög, så lägg snuset på en tidning täckt med hushållspapper för sluttorkning, kan ta ca 2-4 timmar beroende på rumstemperatur. Du förpackar nu snuset i dosor eller askar. Kom ihåg att lagring förhöjer aromen. För att du alltid skall tillgång till färsktsnus skall du förvara det i kylskåp, det g&aring;r &auml;ven bra att frysa
ner det förpackade snuset och vid behov plocka ut dosorna ur frysen.</div>
					</div>
				</center>
			</div> <!-- #content_div -->
			<? include("copyright_div.php"); ?>
		</div> <!-- #page_div -->
	</body>
</html>