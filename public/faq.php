<?
// Logga in i databasen
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

include("loginchecker.php");

mysql_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Vanliga frågor om snusaeget.se : snusaeget.se</title>
		<link href="stilen.css" rel="stylesheet" type="text/css">
		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
	</head>
	<body>
	  <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-827328-10']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
		<div id="page_div">
			<? include("logo_div.php"); ?>
			<? include("menu_div.php"); ?>
			<div id="content_div">
				<center>
				<div style="width:500px; text-align:left; border:1px #006633 dashed; background:#FFFFFF; padding:5px;">
					<div style="font-weight:bold;">- Hur fungerar förskottsbetalningen?</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> Man g&ouml;r en best&auml;llning precis som vanligt, fast man klickar i f&ouml;rskottsbetalning som betalningsalternativ. Sedan n&auml;r ordern har g&aring;tt igenom s&aring; betalar man in ordersumman till <b>BankGiro 5277-7141</b> och skriver ordernummret i meddelande rutan (ordernummret &auml;r RO- f&ouml;ljt av fyra siffror). Sedan skickar vi varorna s&aring; fort pengarna &auml;r registrerade p&aring; v&aring;rt konto, brukar vanligtvis ta 3 bankdagar. P&aring; detta s&auml;tt slipper man betala postf&ouml;rskottsavgiften.</div>
					<div style="font-weight:bold;">- Varför ska man samla poäng? (Bonussystem)</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> Man får ett poäng per krona och poängen går att växla in emot rabatter. (&auml;nnu billigare!) <br>
					1000p = 100kr rabatt<br>
				    2000p = 225kr rabatt<br>
				    5000p = 600kr rabatt</div>
					<div style="font-weight:bold;">- Sidan funkar inte, jag kan inte beställa?</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> Kontrollera ifall Javascripts är avstängda i webläsarinställningarna. Denna sida kräver att javascripts är tillåtet i er webläsare.</div>
					<div style="font-weight:bold;">- Jag vill avbeställa, hur gör jag?</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> Det är enkelt att avbeställa här på snusaeget.se. Logga bara in på "Min sida" med kundnummer och pinkod och klicka på "ta bort"-ikonen(<img src="images/remove.gif" width="10" height="10">) jämnte orderstatusen. Detta går dock endast göra när ordern har statusen obehandlad/obetald, efter det så kan det vara försent, kontakta oss via formuläret på kontakt sidan så ska vi försöka hjälpa dig så gott vi kan.</div>
					<div style="font-weight:bold;">- Vem/vilka står bakom snusaeget.se?</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> snusaeget.se ägs och drivs av Cut´n Roll KB (Org nr. 969661-1632). Cut´n Roll KB har sitt säte i Skara kommun.</div>
					<div style="font-weight:bold;">- Skickar ni till utlandet?</div>
					<div style="padding-left:15px; padding-bottom:10px;"><b>Svar:</b> Nej, vi skickar inte till utlandet.</div>
				</div>
				</center>
			</div> <!-- #content_div -->
			<? include("copyright_div.php"); ?>
		</div> <!-- #page_div -->
	</body>
</html>
