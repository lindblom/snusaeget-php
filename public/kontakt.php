<?
// Logga in i databasen
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

include("loginchecker.php");

mysql_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Kontakta oss : snusaeget.se</title>
		<link href="stilen.css" rel="stylesheet" type="text/css">
		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
		<script language="javascript" type="text/javascript">
		<!--
		function Check()
		{
		   if (document.form.msg.value.length < 1)
		   {
		      alert("Meddelanderutan kan inte lämnas tom.");
		      return false;
		   }
		}
		-->
		</script>
	</head>
	<body>
	  <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-827328-10']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
		<div id="page_div">
			<? include("logo_div.php"); ?>
			<? include("menu_div.php"); ?>
			<div id="content_div">
				<center>
					<div style="width:500px; text-align:left; border:1px #006633 dashed; background:#FFFFFF; padding:5px;">
						<?
						if($_POST['msg']==NULL)
						{
						?>
						<div>Fyll i din fråga i formuläret nedan..</div><br>
						<form action="kontakt.php" method="post" name="form" enctype="application/x-www-form-urlencoded" onSubmit="return Check();">
						<table width="450" cellpadding="2" cellspacing="2" align="center" style="margin-left: auto; margin-right: auto;">
						<tr>
						<td valign="top" width="109">
						Namn:	</td>
						<td width="325">
						<input name="name" type="text" size="25" maxlength="50">
						</td>
						</tr>
						<tr>
						<td valign="top">
						E-post:
						</td>
						<td>
						<input name="mail" type="text" size="25" maxlength="50">
						</td>
						</tr>
						<tr>
						<td valign="top">
						Fr&aring;ga:
						</td>
						<td>
						<textarea name="msg" cols="30" rows="5"></textarea>
						</td>
						</tr>
						<tr>
						<td colspan="2" align="center">
						<input name="" type="submit" value="Skicka">
						</td>
						</tr>
						</table>
						</form>
						<?
						}
						else
						{
              require_once "Mail.php";
              require('mail_settings.php');
               $subject = "[kontakt-form]";
               $body = "Meddelande från: " . $_POST['mail'] . ' (' . $_POST['name'] . ')' . "\n" . $_POST['msg'];

               $headers = array ('From' => $from_header,
                 'To' => $from_header,
                 'Subject' => $subject);

               $mail = $smtp->send($from_header, $headers, $body);

               if (PEAR::isError($mail)) {
                  error_log($mail->toString());
                  echo "Formuläret verkar vara trasigt för stunden, försök gärna lite senare eller skicka din fråga direkt till kontakt@snusaeget.se";
                 } else {
                  echo "Tack för för din fråga, räkna med svar inom 24 timmar.";
                 }
						}
						?>
					</div>
				</center>
			</div> <!-- #content_div -->
			<? include("copyright_div.php"); ?>
		</div> <!-- #page_div -->
	</body>
</html>