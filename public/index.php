<?
// Logga in i databasen
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

include("loginchecker.php");

mysql_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Göra eget snus : snusaeget.se</title>
	<meta name="description" content="snusaeget.se är en webshop för den som vill göra sitt eget snus, vi säljer snussater och snusaromer.">
	<meta name="keywords" content="eget snus, snusa eget, göra snus, snus, tillverka snus, snustillverkning">
	<link href="stilen.css" rel="stylesheet" type="text/css">
	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
	</script>
</head>
	<body>
	  <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-827328-10']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
		<div id="page_div">
			<? include("logo_div.php"); ?>
			<? include("menu_div.php"); ?>
			<div id="content_div">
					<table style="width: 650px; border: 0; margin-left: auto; margin-right: auto; margin-bottom: 5px;">
					<tr>
						<td valign="top" rowspan="2" style="font-weight: bold; width: 150px;">
							<h3>Vi säljer ...</h3>
							&nbsp;<a href="snussatser.php#110"><img src="products/ratobak-blad.jpg" alt="råtobak som lösa blad" style="border: 0;"/></a><br />&nbsp; råtobak som blad.<br /><br />
							&nbsp;<a href="snussatser.php#220"><img src="products/ratobak-pulver.jpg" alt="råtobak som malet pulver" style="border: 0;"/></a><br />&nbsp; råtobak som pulver.
						</td>
						<td valign="top" style="width: 500px;">
						<h3>Nyheter</h3>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;Serverflytt pågår
							</div>
							<div class="news_text">
								<p>
							    Snusaeget kommer flyttas till en ny server, detta kan göra att man hamnar på sidan flyttar.snusaeget.se (som redan ligger på den nya servern) men det går bra att beställa även där.
								</p>
							</div>
						</div>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;Nu går det handla igen.
							</div>
							<div class="news_text">
								<p>
							    Då var sidan uppe igen, allt är som vanligt.
								</p>
							</div>
						</div>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;Vårt webbhotell blev utsatt för en attack.
							</div>
							<div class="news_text">
								<p>
								  Vårt webbhotell har blivit utsatt för en attack, så just nu går det inte handla från snusaeget.se. Eftersom även min epostadress låg på det webbhotell som blev utsatt så når ni nu mig på <a href="mailto:info@cutnroll.com">info@cutnroll.com</a>.<br>
								  Jag har tyvärr inte mer information för stunden, men hoppas att personalen på webbhotellet löser detta så snart som möjligt.
								</p>
							</div>
						</div>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;14 dagar semster
							</div>
							<div class="news_text">
								<p>
								  Från och med Tisdag den 4e Oktober och 14 dagar fram kommer inga ordrar behandlas då jag tar semester. Ordrar som kommer in under den tiden behandlas med raketfart när jag kommer tillbaka.
								</p>
							</div>
						</div>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;Nya fraktpriser
							</div>
							<div class="news_text">
								<p>
								  Vi har nu lagt in de nya faktpriserna. 85kr för försändelser under fyra kg och 125 kr över. Efterkravsavgiften ligger på 60kr. Samtliga inkl moms.
								</p>
							</div>
						</div>
						<div class="news_container">
							<div class="news_date">
							&nbsp;&nbsp;Alla leveranser skickas nu med DHL Service Point.
							</div>
							<div class="news_text">
								<p>
								  Vi har som ni kanske redan märkt lämnat Posten och låter nu DHL Service Point sköta våra leveranser.
								</p>
							</div>
						</div>
						</td>
					</tr>
					</table>
				<div id="warning_div">
					Genom att snusa kan ni skada er hälsa allvarligt.
				</div> <!-- #warning_div -->
			</div> <!-- #content_div -->
			<? include("copyright_div.php"); ?>
		</div> <!-- #page_div -->
	</body>
</html>