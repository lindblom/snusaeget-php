<?
// Logga in i databasen
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

include("loginchecker.php");

mysql_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Köpinformation : snusaeget.se</title>
		<link href="stilen.css" rel="stylesheet" type="text/css">
		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
</head>
	<body>
	  <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-827328-10']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
		<div id="page_div">
			<? include("logo_div.php"); ?>
			<? include("menu_div.php"); ?>
			<div id="content_div">
				<center>
				<div style="width:500px; text-align:left; border:1px #006633 dashed; background:#FFFFFF; padding:5px;">
					<div style="font-weight:bold;">PRIS:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Alla priser är inkl moms.</div>

					<div style="font-weight:bold;">RABATTER:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Vi har ett mycket förmånligt bonussystem, läs mer om det under "vanliga frågor".</div>

					<div style="font-weight:bold;">LEVERANS:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Alla försändelser sker via DHL Service Point. </div>

					<div style="font-weight:bold;">EJ UTLÖSTA LEVERANSER:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Alla IP-adresser loggas och falskbeställningar polisanmäls ovillkorligen som bedrägeri.<br>
					Samtliga ordrar som ej blivit utlösta debiteras med frakt + efterkravsavgift + returfrakt.<br></div>

					<div style="font-weight:bold;">BETALSÄTT:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Vi erbjuder för tillfället två betalningsalternativ.<br>
					&nbsp;&middot;&nbsp; Efterkrav, betalas när försändelsen hämtas. (+50kr)<br>
					&nbsp;&middot;&nbsp; Förskottsbetalning, betalning sker i förväg till vårt BankGiro.</div>

					<div style="font-weight:bold;">FEL/PRISÄNDRINGAR/SLUTFÖRSÄLJNING:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Vi reserverar oss mot skrivfel, prisändringar och slutförsäljning som kan uppstå trots kontinuerlig uppdatering på snusaeget.se.</div>

					<div style="font-weight:bold;">ÅLDERSGRÄNS:</div>
					<div style="padding-left:15px; padding-bottom:10px;">Vi säljer bara till personer <b>över</b> 18år.</div>
				</div>
				</center>
			</div> <!-- #content_div -->
			<? include("copyright_div.php"); ?>
		</div> <!-- #page_div -->
	</body>
</html>