<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>snusaeget.se - Administration</title>
<link rel="shortcut icon" href="favicon.ico">
<script language="javascript" type="text/javascript" src="popup.js">
</script>
</head>
<body>
<?php
require('login.php');
mysql_connect($sqlip,$user,$password);
@mysql_select_db($database) or die( "Unable to select database");

$customer_count = mysql_query("SELECT COUNT(*) from kunder");
$orders_status_0_pf = mysql_query("SELECT * FROM orders WHERE (status = 0) AND (payment = 'pf') ORDER BY order_nr ASC;");
$orders_status_0_bg = mysql_query("SELECT * FROM orders WHERE (status = 0) AND (payment = 'bg') ORDER BY order_nr ASC;");
$orders_status_1 = mysql_query("SELECT * FROM orders WHERE status = 1 ORDER BY order_nr ASC;");
$orders_status_2 = mysql_query("SELECT * FROM orders WHERE status = 2 ORDER BY shipping_date DESC LIMIT 0, 50;");
$orders_status_3 = mysql_query("SELECT * FROM orders WHERE status = 3 ORDER BY order_nr DESC;");
mysql_close();
?>

<div style="text-align: center;">- <a href="index.php">Orderöversikt</a> | Antal kundnummer: <? echo mysql_result($customer_count, 0, "count(*)"); ?> | <a href="mail.php">Epostadresser</a>
</div>

<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="600" colspan="4" style="font-size:22px;">Obehandlade (Förskottsbetalning)</td>
	</tr>
	<tr>
		<td width="144" style="border-bottom:1px #000000 solid;">Ordernr:</td>
		<td width="145" style="border-bottom:1px #000000 solid;">Kundnr:</td>
		<td width="221" style="border-bottom:1px #000000 solid;">Orderdatum:</td>
		<td width="90" style="border-bottom:1px #000000 solid;">- OK -</td>
	</tr>
	<?php
	for($n=0; $n<mysql_numrows($orders_status_0_bg); $n++)
	{
		echo "<tr onmouseover=\"this.style.backgroundColor='#F0F0F0';\" onmouseout=\"this.style.backgroundColor='';\"><td><a href=\"\" onclick=\"popUpWindow('show_order.php?order_nr=" . mysql_result($orders_status_0_bg, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_bg, $n, "pin") . "',200,100,750,700);return false;\">RO-" . mysql_result($orders_status_0_bg, $n, "order_nr") . "</a></td><td><a href=\"\" onclick=\"popUpWindow('show_kund.php?kund_nr=" . mysql_result($orders_status_0_bg, $n, "kund_nr") . "',200,100,320,160);return false;\">RK-" . mysql_result($orders_status_0_bg, $n, "kund_nr") . "</a></td><td>" . mysql_result($orders_status_0_bg, $n, "order_date") . "</td><td><button onClick=\"if(window.confirm('Är du säker?')) window.location = 'advance_order.php?order_nr=" . mysql_result($orders_status_0_bg, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_bg, $n, "pin") . "&status=0';\">OK</button><button onClick=\"javascript:if(window.confirm('Vill du annulera ordern?')) window.location = 'kill_order.php?order_nr=" . mysql_result($orders_status_0_bg, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_bg, $n, "pin") . "';\">Kill</button></td></tr>";
	}
	?>
</table>
&nbsp;<br>

<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="600" colspan="4" style="font-size:22px;">Obehandlade (Efterkrav)</td>
	</tr>
	<tr>
		<td width="144" style="border-bottom:1px #000000 solid;">Ordernr:</td>
		<td width="145" style="border-bottom:1px #000000 solid;">Kundnr:</td>
		<td width="221" style="border-bottom:1px #000000 solid;">Orderdatum:</td>
		<td width="90" style="border-bottom:1px #000000 solid;">- OK -</td>
	</tr>

	<?php
	for($n=0; $n<mysql_numrows($orders_status_0_pf); $n++)
	{
		echo "<tr onmouseover=\"this.style.backgroundColor='#F0F0F0';\" onmouseout=\"this.style.backgroundColor='';\"><td><a href=\"\" onclick=\"popUpWindow('show_order.php?order_nr=" . mysql_result($orders_status_0_pf, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_pf, $n, "pin") . "',200,100,750,700);return false;\">RO-" . mysql_result($orders_status_0_pf, $n, "order_nr") . "</a></td><td><a href=\"\" onclick=\"popUpWindow('show_kund.php?kund_nr=" . mysql_result($orders_status_0_pf, $n, "kund_nr") . "',200,100,320,160);return false;\">RK-" . mysql_result($orders_status_0_pf, $n, "kund_nr") . "</a></td><td>" . mysql_result($orders_status_0_pf, $n, "order_date") . "</td><td><button onClick=\"if(window.confirm('Är du säker?')) window.location = 'advance_order.php?order_nr=" . mysql_result($orders_status_0_pf, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_pf, $n, "pin") . "&status=0';\">OK</button><button onClick=\"javascript:if(window.confirm('Vill du annulera ordern?')) window.location = 'kill_order.php?order_nr=" . mysql_result($orders_status_0_pf, $n, "order_nr") . "&pin=" . mysql_result($orders_status_0_pf, $n, "pin") . "';\">Kill</button></td></tr>";
	}
	?>
</table>
&nbsp;<br>

<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="600" colspan="4" style="font-size:22px;">Underbehandling</td>
	</tr>
	<tr>
		<td width="144" style="border-bottom:1px #000000 solid;">Ordernr:</td>
		<td width="145" style="border-bottom:1px #000000 solid;">Kundnr:</td>
		<td width="221" style="border-bottom:1px #000000 solid;">Orderdatum:</td>
		<td width="90" style="border-bottom:1px #000000 solid;">- OK -</td>
	</tr>

	<?php
	for($n=0; $n<mysql_numrows($orders_status_1); $n++)
	{
		echo "<tr onmouseover=\"this.style.backgroundColor='#F0F0F0';\" onmouseout=\"this.style.backgroundColor='';\"><td><a href=\"\" onclick=\"popUpWindow('show_order.php?order_nr=" . mysql_result($orders_status_1, $n, "order_nr") . "&pin=" . mysql_result($orders_status_1, $n, "pin") . "',200,100,750,700);return false;\">RO-" . mysql_result($orders_status_1, $n, "order_nr") . "</a></td><td><a href=\"\" onclick=\"popUpWindow('show_kund.php?kund_nr=" . mysql_result($orders_status_1, $n, "kund_nr") . "',200,100,320,160);return false;\">RK-" . mysql_result($orders_status_1, $n, "kund_nr") . "</a></td><td>" . mysql_result($orders_status_1, $n, "order_date") . "</td><td><button onClick=\"if(window.confirm('Är allt klart?')){ var y=window.prompt('Fyll i kolli-id om det finns:');{} window.location = 'advance_order.php?order_nr=" . mysql_result($orders_status_1, $n, "order_nr") . "&pin=" . mysql_result($orders_status_1, $n, "pin") . "&status=1&shipping_ref=' + y;};\">OK</button></td></tr>";
	}
	?>
</table>
&nbsp;<br>

<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="4" style="font-size:22px;">De 25 senast klara</td>
	</tr>
	<tr>
		<td width="144" style="border-bottom:1px #000000 solid;">Ordernr:</td>
		<td width="145" style="border-bottom:1px #000000 solid;">Kundnr:</td>
		<td width="155" style="border-bottom:1px #000000 solid;">Orderdatum:</td>
		<td width="156" style="border-bottom:1px #000000 solid;">Utskicksdatum:</td>
	</tr>
	<?php
	for($n=0; $n<mysql_numrows($orders_status_2); $n++)
	{
		echo "<tr onmouseover=\"this.style.backgroundColor='#F0F0F0';\" onmouseout=\"this.style.backgroundColor='';\"><td><a href=\"\" onclick=\"popUpWindow('show_order.php?order_nr=" . mysql_result($orders_status_2, $n, "order_nr") . "&pin=" . mysql_result($orders_status_2, $n, "pin") . "',200,100,750,700);return false;\">RO-" . mysql_result($orders_status_2, $n, "order_nr") . "</a>";
		if(strlen(mysql_result($orders_status_2, $n, "shipping_ref"))>1)
			echo " <span style\"font-size: 80%\">[<a href=\"http://servicepoint.se/Sp%C3%A5rapaket/tabid/495/queryConsNo/" . mysql_result($orders_status_2, $n, "shipping_ref") . "/Default.aspx\" target=\"_blank\">spåra</a>]</span>";

		echo "</td><td><a href=\"\" onclick=\"popUpWindow('show_kund.php?kund_nr=" . mysql_result($orders_status_2, $n, "kund_nr") . "',200,100,320,160);return false;\">RK-" . mysql_result($orders_status_2, $n, "kund_nr") . "</a></td><td>" . mysql_result($orders_status_2, $n, "order_date") . "</td><td>" . mysql_result($orders_status_2, $n, "shipping_date") . "</td></tr>";
	}
	?>
</table>
&nbsp;<br>

<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="4" style="font-size:22px;">Annulerade</td>
	</tr>
	<tr>
		<td width="144" style="border-bottom:1px #000000 solid;">Ordernr:</td>
		<td width="145" style="border-bottom:1px #000000 solid;">Kundnr:</td>
		<td width="155" style="border-bottom:1px #000000 solid;">Orderdatum:</td>
		<td width="156" style="border-bottom:1px #000000 solid;"></td>
	</tr>
	<?php
	for($n=0; $n<mysql_numrows($orders_status_3); $n++)
	{
		echo "<tr onmouseover=\"this.style.backgroundColor='#F0F0F0';\" onmouseout=\"this.style.backgroundColor='';\"><td><a href=\"\" onclick=\"popUpWindow('show_order.php?order_nr=" . mysql_result($orders_status_3, $n, "order_nr") . "&pin=" . mysql_result($orders_status_3, $n, "pin") . "',200,100,750,700);return false;\">RO-" . mysql_result($orders_status_3, $n, "order_nr") . "</a></td><td><a href=\"\" onclick=\"popUpWindow('show_kund.php?kund_nr=" . mysql_result($orders_status_3, $n, "kund_nr") . "',200,100,320,160);return false;\">RK-" . mysql_result($orders_status_3, $n, "kund_nr") . "</a></td><td>" . mysql_result($orders_status_3, $n, "order_date") . "</td><td>&nbsp;</td></tr>";
	}
	?>
</table>
</body>
</html>
